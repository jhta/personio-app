import React from 'react'
import { DropTarget } from 'react-dnd'

const employeeListTarget = {
  drop (props, monitor) {
    props.replace({ parent: props.parent })
    console.log(props)
  }
}

const collect = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver()
})

const DraggableZone = ({ connectDropTarget, isOver }) => connectDropTarget(
  <section>
    <div>
      <span>drag here</span>
    </div>
    <style jsx>{`
      section {
        height: 140px;
        min-width: 150px;
        width: 100%;
        left: 0;
        background: ${isOver ? '#bfeef4db' : 'transparent'};
        z-index: ${isOver ? '15' : '0'};
        position: absolute;
        box-sizing: border-box;
        display: flex;
        padding: .5rem;
      }
      div {
        border: ${isOver ? '2px white dashed' : 'none'};
        border-radius: 5px;
        width: 100%;
        height: 100%;
        justify-content: center;
        align-items: center;
        display: flex;
      }
      span {
        display: ${isOver ? 'inline-block' : 'none'};
        font-size: 2rem;
        font-weight: bold;
        color: white;
        font-family: sans-serif;
      }
      `}</style>
  </section>
)

export default DropTarget('Employee', employeeListTarget, collect)(DraggableZone)
