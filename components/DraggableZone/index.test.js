import React from 'react'
import { shallow } from 'enzyme'
import DraggableZoneWrapper from './index'

describe('<DraggableZone />', () => {
  const DraggableZone = DraggableZoneWrapper.DecoratedComponent
  const wrapper = shallow(<DraggableZone connectDropTarget={e => e} />)

  test('should be render with default props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })
})
