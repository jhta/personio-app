import React from 'react'
import PropTypes from 'prop-types'

const EmployeeInfo = ({ name = '', position = '' }) => (
  <div id='EmployeeInfo'>
    <p className='name'>{`Name: ${name}`}</p>
    <p className='position'>{`Position: ${position}`}</p>
    <style jsx>{`
      #EmployeeInfo {
        border: 1px solid #ccc;
        padding: .5rem;
        margin: 1rem;
        border-radius: 5px;
        z-index: 10;
        cursor: pointer;
        box-shadow: 0px 0px 6px 0px #0000002b;
      }
      `}</style>
  </div>
)

EmployeeInfo.propTypes = {
  name: PropTypes.string,
  position: PropTypes.string
}

export default EmployeeInfo
