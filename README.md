# Personio App

Code Challengue for [@personiohr](https://twitter.com/personiohr)

## DEMO
https://personio-app-ybwvxoovix.now.sh

## Stack
* [Next.js](): It's used for SSR, easy babel/webpack configuration
* [Rematch](https://github.com/rematch/rematch): Easy redux implementation for manage the state
* [React Dropzone](react-dropzone.js.org): It's used For load the JSON File with drag and drop
* [React DND](https://github.com/react-dnd/react-dnd): Used for D&D actions
* [Jest](jestjs.io): JS Testing framework. 
* [Enzyme](https://github.com/airbnb/enzyme): Testing Utility for React
* [Docz](https://www.docz.site/): used for documentation. MDX docs easy 
* [Standard](standardjs.com): Used for define a linter styleguide
## Requirements
* Node >  = 9.4.0
* Yarn >= 1.7.0
* Docker (If you want to run the image)

## Scripts

* Install libraries: `yarn install`
* Run dev server: `yarn dev`
* Build for production: `yarn build`
* Start production server: `yarn start`
* Run Linter: `yarn lint`
* Fix linter errors: `yarn lint:fix`
* Run tests: `yarn test`
* Create coverage report: `yarn test:coverage`
* Build Docker image(personio-app): `yarn build:image`
* Run Docker image: `yarn start:image` 
* Run docs server: `yarn docs`
* Create docs: `yarn docs:build`
  

## Run local env:
* `localhost:3000`
## Integration tests 

For the project it is running `Jest/Enzyme` test enviorment.

Jest by default process the `*.test.js` and `*.spec.js`.
We recommend add the file: `[file-name].test.js`  in the same folder for components and libraries.

Coverage report is generated on folder: `/coverage/`

* For run the test: `yarn tests` 
* For run watch tests: `yarn test --watch`
* For generate test coverage report: `yarn test:coverage`

## Docker image

image: https://hub.docker.com/r/mhart/alpine-node/
* Build image: `yarn build:image`
* Run image: `yarn build:image`

Test it: `localhost:3000`

## Documentation 
For documentation it use use [docz](https://docz.site)

For run the docz server:

`yarn docs`

For build the docs static website:

`yarn docs:build`

this create `static/docs` folder when you can find a `index.html` entry point

## Pre push hook

Hook created for run test before to `git push`. For more details check the folder `/scripts`.

for install the hook:
* Make executable the scripts:
  `chmod +x scripts/run-tests.bash scripts/pre-push.bash scripts/install-hooks.bash`
* Run the installer script:
* `./scripts/install-hooks.bash`

