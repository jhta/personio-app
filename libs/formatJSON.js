import HierarchyMap from './HierarchyMap'
/**
 * Factory method for HierarchyMap, used for protect the direct access to private
 * vars and methods
 * @param {Object} data object data parsed from .json file
 * @return {Object} method to access to HierarchyMap
 * */
function getFormatedJSON (data) {
  const Hierarchy = new HierarchyMap(data)

  if (!HierarchyMap) {
    return {
      error: { message: 'HierarchyMap cannot created' }
    }
  }

  return {
    get: () => Hierarchy.get(),
    clear: () => Hierarchy.clear(),
    getTree: () => Hierarchy.getTree(),
    replaceEmployee: (name, newBoss) => Hierarchy.replaceEmployee(name, newBoss),
    collapse: (name) => Hierarchy.collapse(name)
  }
}

export default getFormatedJSON
