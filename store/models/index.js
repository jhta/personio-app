import getHierarchyMap from 'libs/formatJSON'

export const hierarchyTree = {
  state: {
    hierarchyMap: null,
    tree: null,
    draggableNode: null,
    error: null
  },
  reducers: {
    create: (state, payload) => {
      const hierarchyMap = getHierarchyMap(payload)
      return ({
        hierarchyMap,
        tree: hierarchyMap.getTree()
      })
    },

    dragging: (state, payload) => ({ ...state, draggableNode: payload }),

    replace: ({ draggableNode = {}, hierarchyMap, tree }, { parent }) => {
      try {
        return (!draggableNode.parent || draggableNode.parent === parent)
          ? ({
            hierarchyMap,
            tree,
            draggableNode: null
          })
          : ({
            draggableNode: null,
            tree: hierarchyMap.replaceEmployee(draggableNode.name, parent),
            hierarchyMap
          })
      } catch (error) {
        return ({
          draggableNode,
          hierarchyMap,
          tree,
          error
        })
      }
    },

    collapse: ({ hierarchyMap }, name) => ({
      draggableNode: null,
      tree: hierarchyMap.collapse(name),
      hierarchyMap
    }),

    removeError: (state) => ({
      ...state,
      error: null
    })
  }
}
