import React from 'react'
import PropTypes from 'prop-types'
import { DragSource } from 'react-dnd'
import EmployeeList from '../EmployeeList'
import EmployeeInfo from '../EmployeeInfo'
import DraggableZone from '../DraggableZone'

const employeeSource = {
  beginDrag (props) {
    props.dragging(props.employee)
    console.log(props)
    return {}
  }
}

const collect = (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
})

const Employee = ({ employee, connectDragSource, dragging, replace, collapse }) =>
  connectDragSource(
    <section>
      <div>
        <EmployeeInfo name={employee.name} position={employee.position} />
        {
          employee.parent && <DraggableZone parent={employee.parent} replace={replace} />
        }
      </div>
      <EmployeeList
        employees={employee.employees}
        boss={employee.name}
        dragging={dragging}
        replace={replace}
        collapse={collapse}
        showList={!employee.collapsed}
      />
      <style jsx>{`
        section {
          min-width: 150px;
          display: flex;
          flex-direction: column;
          align-items: center;
          position: relative;
        }
        div {
          display: flex;
        }
        `}</style>
    </section>
  )

Employee.propTypes = {
  employee: PropTypes.object.isRequired,
  connectDragSource: PropTypes.func.isRequired,
  dragging: PropTypes.func.isRequired,
  collapse: PropTypes.func.isRequired,
  replace: PropTypes.func.isRequired,
  showDragZone: PropTypes.bool
}

Employee.defaultProps = {
  employee: {}
}

export default DragSource('Employee', employeeSource, collect)(Employee)
