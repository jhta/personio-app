import React from 'react'
import { shallow } from 'enzyme'
import EmployeeWrapper from './index'
import EmployeeInfo from '../EmployeeInfo'
import DraggableZone from '../DraggableZone'

describe('<Employee>', () => {
  const Employee = EmployeeWrapper.DecoratedComponent
  let wrapper = null
  beforeEach(() => {
    wrapper = shallow(
      <Employee
        connectDragSource={c => c}
        collapse={() => {}}
        replace={() => {}}
        dragging={() => {}}
      />
    )
  })

  test('should render with required props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  test('should not render <DraggableZone /> component', () => {
    expect(wrapper.find(DraggableZone).exists()).toBeFalsy()
  })

  test('should be render <EmployeeInfo /> component', () => {
    expect(wrapper.find(EmployeeInfo).exists()).toBeTruthy()
  })

  describe('setting employee prop', () => {
    test('should be render <DraggableZone /> component with employee.parent prop', () => {
      wrapper.setProps({ employee: { position: 'Dev', name: 'David', parent: 'Juan' } })
      expect(wrapper.find(DraggableZone).exists()).toBeTruthy()
    })

  })
})
