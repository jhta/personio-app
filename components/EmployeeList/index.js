import React from 'react'
import PropTypes from 'prop-types'
import Fade from 'react-reveal/Fade'
import Employee from '../Employee'

const EmployeeList = ({ employees = [], boss = '', dragging, replace, collapse, showList }) => !employees.length
  ? null
  : (
    <section id={boss} className='EmployeeList'>
      <span>
        <span>{`${boss} employees (${employees.length})`}</span>
        <span
          className='title'
          onClick={() => { collapse(boss) }}
        >
          {showList ? 'hide' : 'show'}
        </span>
      </span>
      {
        showList
          ? (
            <Fade big>
              <div className='EmployeeList__employees'>
                {
                  employees.map((employee, index) => (
                    <Employee
                      employee={employee}
                      key={index}
                      dragging={dragging}
                      replace={replace}
                      collapse={collapse}
                    />
                  ))
                }
              </div>
            </Fade>
          )
          : null
      }
      <style jsx>{`
        section.EmployeeList {
          width: 100%;
          box-sizing: border-box;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
        div.EmployeeList__employees {
          display: flex;
          justify-content: center;
        }

        .title {
          text-decoration: underline;
          padding-left: .5rem;
        }

        .title:hover {
          cursor: pointer;
        }
        `}</style>
    </section>
  )

EmployeeList.propTypes = {
  employees: PropTypes.array,
  boss: PropTypes.string,
  dragging: PropTypes.func.isRequired,
  collapse: PropTypes.func.isRequired,
  replace: PropTypes.func.isRequired,
  showList: PropTypes.bool
}

export default EmployeeList
