import React from 'react'
import { shallow } from 'enzyme'
import EmployeeInfo from '../EmployeeInfo'

describe('<EmployeeInfo>', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallow(
      <EmployeeInfo />
    )
  })

  test('should render with default props', () => {
    expect(wrapper.exists()).toBeTruthy()
  })

  test('should render  empty values for Name and Position', () => {
    expect(wrapper.find('.name').text()).toEqual('Name: ')
    expect(wrapper.find('.position').text()).toEqual('Position: ')
  })

  test('should render values Name and Position with props', () => {
    wrapper.setProps({ name: 'Steve', position: 'Dev' })

    expect(wrapper.find('.position').text()).toEqual('Position: Dev')
    expect(wrapper.find('.name').text()).toEqual('Name: Steve')
  })
})
