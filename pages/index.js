import React from 'react'
import FileLoader from 'components/FileLoader'
import MapContainer from 'components/MapContainer'
import { Provider } from 'react-redux'
import store from '../store'

class Page extends React.Component {
  constructor () {
    super()
    this.state = {
      files: [],
      data: null,
      message: 'Try dropping some files here, or click to select files to upload.'
    }
    this.getJSONData = this.getJSONData.bind(this)
  }

  getJSONData (err, data) {
    this.setState({
      data,
      message: err
    })
  }

  render () {
    const { message, data } = this.state
    return (
      <Provider store={store}>
        <React.Fragment>
          <FileLoader message={message} enable={!data} setData={this.getJSONData} />
          <MapContainer />
        </React.Fragment>
      </Provider>
    )
  }
}

export default Page
