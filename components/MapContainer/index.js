import React from 'react'
import PropTypes from 'prop-types'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import Fade from 'react-reveal/Fade'
import { connect } from 'react-redux'
import Employee from '../Employee'
import ErrorBox from '../ErrorBox'

const MapContainer = ({ tree = {}, dragging, replace, collapse, error, onClose }) =>
  !error && !tree
    ? null
    : (
      <Fade>
        <section>
          {
            error
              ? <ErrorBox error={error} onClose={onClose} />
              : null
          }
          <Employee
            employee={tree}
            dragging={dragging}
            replace={replace}
            collapse={collapse}
          />
        </section>
      </Fade>
    )

MapContainer.propTypes = {
  tree: PropTypes.object,
  dragging: PropTypes.func.isRequired,
  collapse: PropTypes.func.isRequired,
  replace: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  error: PropTypes.object
}

const mapState = ({ hierarchyTree: { tree, error } }) => ({
  tree,
  error
})

const mapDispatch = ({ hierarchyTree: { dragging, replace, collapse, removeError } }) => ({
  dragging,
  replace,
  collapse,
  onClose: (e) => { removeError() }
})

export default DragDropContext(HTML5Backend)(
  connect(mapState, mapDispatch)(
    MapContainer
  )
)
