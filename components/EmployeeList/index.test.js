import React from 'react'
import { shallow } from 'enzyme'
import EmployeeList from './index'
import Employee from '../Employee'

const mockEmploy = { name: 'Steve', position: 'dev', parent: 'Richard' }

describe('<EmployeeList />', () => {
  let wrapper = null
  beforeEach(() => {
    wrapper = shallow(
      <EmployeeList
        collapse={() => {}}
        replace={() => {}}
        dragging={() => {}}
      />
    )
  })

  test('should not render with required props', () => {
    expect(wrapper.find('section').exists()).toBeFalsy()
  })

  test('should be render if have employees prop', () => {
    wrapper.setProps({ employees: [mockEmploy] })
    expect(wrapper.exists()).toBeTruthy()
  })

  test('should be render the number of employees spent on props when show list is true', () => {
    wrapper.setProps({ employees: [mockEmploy], showList: true })
    expect(wrapper.find(Employee).length).toBe(1)
  })

  // I would like try the collapse option... action options

})
