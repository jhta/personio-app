/**
 * @class HierarchyMap
 * Class for manage the employees hierarchy map as dictionary(n(1)) and
 * provide methods for data manipulation
 * @param {Object} data default data readed for the JSON file
 * @param {String} this._boss "private" attribute boss
 * @param {Map} this._dict "private" Map data object
 * @param {Object} this._tree private tree object representation
 */
class HierarchyMap {
  /**
   * @function
   * Constructor method for HierarchyMap class, receive a object
   * Set default private vars
   * Create a dictionary with every employee as keys
   * Create a tree representation for use in the React components tree
   */
  constructor (data) {
    this._boss = Object.keys(data)[0] || null
    this._dict = new Map()

    const bossData = data[this._boss]
    this._dict.set(this._boss, this.parseEmployee(bossData, this._boss))
    this.buildDict(bossData, this._boss)
    this._tree = this.buildTree()
  }

  /**
   * @function
   * Method for parse employee information to add as value in dict
   * @param {String} [employee].position position
   * @param {Array} [employee].employees direct employees
   * @param {String} parent parent key in dict
   */
  parseEmployee ({ position, employees }, name, parent) {
    return {
      name,
      position,
      parent,
      employees: employees.map(em => Object.keys(em)[0])
    }
  }

  /*
   * @function
   * Method for get private var boss
   * @return {String} boss private value
   */
  getBoss () {
    return this._boss
  }

  /*
   * Methor fort get a clone tree representation
   * @return {Object} tree representation
   */
  getTree () {
    return { ...this._tree }
  }

  /**
   * Method for get dictionary
   * @return {Map} hierarchy Map clone
   */
  get () {
    return new Map(this._dict)
  }

  /**
   * Method for build tree representation based on the dict info
   * @return {Object} tree
   */
  buildTree () {
    const boss = this._dict.get(this._boss)
    return {
      ...boss,
      employees: boss.employees.map(em => this.buildTreeItem(em, this._dict))
    }
  }

  /**
   * Recursive method for iterate inside employ - employees and get the info for
   * anyone from the dict
   * @param {String} name employee key in dict
   * @param {Map} dict dictionary with all user information
   * @return {Object} object represetnation for employee
   */
  buildTreeItem (name, dict) {
    const employee = dict.get(name)
    if (!employee) return 'Error'

    const { employees = [] } = employee
    if (!employees.length) return employee

    return {
      ...employee,
      employees: employees.map(em => this.buildTreeItem(em, dict))
    }
  }

  /*
   * Method for clear the private vars
   * @return {String} message confirmation
   */
  clear () {
    this._dict = new Map()
    this._boss = null
    this._tree = null
    return 'done'
  }

  /**
   * Method for replace/move a employee position, for that it
   * take the node name and change the current parent, take the old
   * parent and remove the node key, take the new parent and add the node keoy
   * @param {String} name key node to replace
   * @param {String} newBoss key node to add the the node
   */
  replaceEmployee (name, newBoss) {
    if (name === newBoss) throw new Error('you cannot assign himself as boss')
    if (name === this._boss) throw new Error('you cannot move the boss')
    if (!newBoss || !this._dict.has(newBoss)) throw new Error('boss not found')
    const employee = this._dict.get(name)

    if (!employee) throw new Error('employee not found')
    if (employee.employees.find(em => em === newBoss)) throw new Error('you cannot assign the boss to a direct employee')

    const parent = this._dict.get(employee.parent)
    const newParent = this._dict.get(newBoss)

    // for the old parent, remve the key
    this._dict.set(
      employee.parent,
      {
        ...parent,
        employees: parent.employees.filter(em => em !== name)
      }
    )

    // for the new parent, add the key
    this._dict.set(
      newBoss,
      {
        ...newParent,
        employees: [...newParent.employees, name]
      }
    )

    // for the node, change the parent name
    this._dict.set(
      name,
      {
        ...employee,
        parent: newBoss
      }
    )

    // re create the tree
    this._tree = this.buildTree()
    return { ...this._tree }
  }

  /**
   * Method for build the initial dictionary iterating recursively the object
   * and setting for every employee the name as key and the object as value
   * this method NOT return nothing, this modifiy the private _dict
   * @param {Array} [employ].employees employ employees to itetare
   * @param {String} parent parent name
   */
  buildDict ({ employees }, parent) {
    if (!employees.length) return 'finished'

    employees.forEach(em => {
      const name = Object.keys(em)[0]
      const employeeData = em[name]
      this._dict.set(name, this.parseEmployee(employeeData, name, parent))
      this.buildDict(employeeData, name)
    })
  }

  collapse (name) {
    const employee = this._dict.get(name)
    if (!employee) {
      return { ...this._tree }
    }

    this._dict.set(
      name,
      {
        ...employee,
        collapsed: !employee.collapsed
      }
    )

    this._tree = this.buildTree()
    return {
      ...this._tree
    }
  }
}

export default HierarchyMap
