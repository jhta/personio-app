import React from 'react'
import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'
import jsonLoader from 'libs/jsonLoader'
import { connect } from 'react-redux'

const dropzoneStyle = {
  width: '100%',
  height: '100%',
  minHeight: 'calc(500px - 2rem)',
  border: '5px dashed white',
  borderRadius: '1rem',
  padding: '1rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  boxSizing: 'border-box',
  cursor: 'pointer'
}

const activeStyle = {
  background: '#ffffff45'
}

class FileLoader extends React.Component {
  constructor (props) {
    super(props)
    this.onDrop = this.onDrop.bind(this)
  }

  async onDrop (files = []) {
    try {
      const { createHierarchyTree } = this.props
      const data = await jsonLoader(files[0])
      createHierarchyTree(data)
    } catch (error) {
      this.props.setData(error.message, null)
    }
  }

  render () {
    const { message, enable } = this.props
    if (!enable) return null

    return (
      <section>
        <Dropzone onDrop={this.onDrop} style={dropzoneStyle} activeStyle={activeStyle}>
          <p>{message}</p>
        </Dropzone>
        <style jsx>{`
          section {
            width: 100%;
            height: 100%;
            min-height: 500px;
            padding: 3rem;
            color: white;
            font-size: 2rem;
            box-sizing: border-box;
            background-color: #8EC5FC;
            background-image: -webkit-linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
            background-image: -moz-linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
            background-image: -o-linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
            background-image: linear-gradient(62deg, #8EC5FC 0%, #E0C3FC 100%);
          }
         `}</style>
      </section>
    )
  }
}

FileLoader.propTypes = {
  message: PropTypes.string,
  setData: PropTypes.func,
  enable: PropTypes.bool
}

const mapStateToProps = ({ hierarchyTree: ht }) => ({
  enable: !ht.hierarchyMap
})

const mapDispatchToProps = ({ hierarchyTree }) => ({
  createHierarchyTree: (data) => hierarchyTree.create(data)
})

export default connect(mapStateToProps, mapDispatchToProps)(FileLoader)
