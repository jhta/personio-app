const FileReader = (typeof window !== 'undefined') ? window.FileReader : {}

const jsonLoader = (file = null) => new Promise((resolve, reject) => {
  const reader = new FileReader()

  reader.onload = ({ target: { result } }) => {
    resolve(JSON.parse(result))
  }

  if (!file) {
    reject(new Error('file not attached.'))
  } else if (file.type !== 'application/json') {
    reject(new Error('Only `.json` files are acceptable.'))
  } else {
    reader.readAsText(file)
  }
})

export default jsonLoader
