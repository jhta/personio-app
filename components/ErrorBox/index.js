import React from 'react'
import Fade from 'react-reveal/Fade'

const ErrorBox = ({ error, onClose }) => error && (
  <Fade bottom>
    <div className='Error'>
      <span>{ 'Error: ' + error.message }</span>
      <span onClick={onClose} className='Error__close'>X</span>
      <style jsx>{`
        .Error {
          position: fixed;
          top: 2rem;
          left: 2rem;
          font-size: 1rem;
          color: white;
          background: tomato;
          z-index: 100;
          padding: 1rem;
        }
        .Error__close {
          box-sizing: border-box;
          padding-left: 1rem;
          cursor: pointer;
          font-weight: bold;
        }
        .Error__close:hover {
          text-decoration: underline;
        }
        `}
      </style>
    </div>
  </Fade>
)

export default ErrorBox
