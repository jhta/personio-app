import React from 'react'
import { shallow } from 'enzyme'
import MapContainerWrapper from './index'
import Employee from '../Employee'
import store from 'store'

const mockTree = { name: 'Steve', position: 'dev', parent: 'Richard' }

describe('<MapContainer />', () => {
  let wrapper = null
  const MapContainer = MapContainerWrapper.DecoratedComponent
  beforeEach(() => {
    wrapper = shallow(
      <MapContainer
        collapse={() => {}}
        replace={() => {}}
        dragging={() => {}}
        store={store}
      />
    )
  })

  test('should not render with required props', () => {
    expect(wrapper.find('section').exists()).toBeFalsy()
  })

  test('should be render if have "tree" prop', () => {
    wrapper.setProps({ tree: mockTree })
    expect(wrapper.exists()).toBeTruthy()
  })

})
