import HierarchyMap from './index'

const mock = '{"Jonas":{"position":"CTO","employees":[{"Sophie":{"position":"VP Engineering","employees":[{"Nick":{"position":"Team Lead","employees":[{"Pete":{"position":"Backend Engineer","employees":[]}},{"Barbara":{"position":"Fronted Engineer","employees":[]}}]}}]}}]}}'

describe('HierarchyMap class', () => {

  const data = JSON.parse(mock)
  let HMap = null
  beforeEach(() => {
    HMap = new HierarchyMap(data)
  })

  test('should create as dictionary map', () => {
    const dict = HMap.get()
    expect(dict).toBeDefined()
    expect(dict.has('Jonas')).toBeTruthy()
  })

  test('should create correctly dictionary with formatted values', () => {
    const dict = HMap.get()
    const sophie = dict.get('Sophie')
    expect(sophie.employees).toEqual(['Nick'])
    expect(sophie.name).toEqual('Sophie')
    expect(sophie.position).toEqual('VP Engineering')
  })

  test('should move correctly a employee to the boss selected with .replaceEmployee', () => {
    const newtree = HMap.replaceEmployee('Barbara', 'Sophie')
    const dict = HMap.get()

    const barbara = dict.get('Barbara')
    const sophie = dict.get('Sophie')

    expect(barbara.parent).toEqual('Sophie')
    expect(sophie.employees.length).toBe(2)
    expect(sophie.employees.find(em => em === 'Barbara')).toBeDefined()
  })

  test('should add collapsed flag with .collapse(name)', () => {
    let collapsed = HMap.collapse('Nick')
    let dict = HMap.get()

    let nick = dict.get('Nick')

    expect(nick.collapsed).toBeTruthy()

    collapsed = HMap.collapse('Nick')
    dict = HMap.get()
    nick = dict.get('Nick')
    expect(nick.collapsed).toBeFalsy()
  })

  test('should return a tree with .getTree()', () => {
    const tree = HMap.getTree()
    expect(tree.name).toEqual('Jonas')
    expect(tree.employees[0].employees[0].parent).toEqual('Sophie')
  })
})
